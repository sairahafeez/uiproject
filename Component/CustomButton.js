import React, { Component } from 'react'
import {View,Text,TouchableOpacity,StyleSheet} from 'react-native'

export default  (CustomButton =({text,color})=>  {
        return (
            <View style={{flex:0.33}}>
                <TouchableOpacity style={[styles.segmentedButton,{backgroundColor:color}]}>
                    <Text style={styles.text}>{text}</Text>
                </TouchableOpacity>
            </View>
        )
    }
)
const styles = StyleSheet.create({
    text:{
        fontSize:18,
        color:'#89B1C2'
    },
    segmentedButton:{
        height:45,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:6
    }
  });