import React, { Component } from 'react'
import {View,Text,TextInput,StyleSheet} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import Icons from 'react-native-vector-icons/Entypo'

export default  (CustomContent =({date})=>  {
        return (
            <View style={styles.headContainer}>
                <View style={{flex:0.8,alignItems:'flex-start',justifyContent:'center',marginLeft:15}}>
                    <View style={{justifyContent:'center',alignItems:'center'}}>
                        <Text style={{fontSize:25,fontWeight:'bold'}}>{date}</Text>
                    </View>
                    <View style={{flexDirection:'row',justifyContent:'center'}}>
                        <Text style={styles.text}>4 Booked</Text>
                        <Icons name={'dot-single'} size={20} style={{alignSelf:'baseline',color:'#88878C'}}/>
                        <Text style={styles.text}>4 Available</Text>
                        <Icons name={'dot-single'} size={20} style={{alignSelf:'baseline',color:'#88878C'}}/>
                        <Text style={styles.text}>2 Reserved</Text>
                    </View>
                </View>
                <View style={{flex:0.2,justifyContent:'center',alignItems:'flex-end',marginRight:10}}>
                    <Icon style={{color:'#C4C3C8'}} name={'ios-arrow-forward'} size={28}/>
                </View>
            </View>
        )
    }
)
const styles = StyleSheet.create({
    headContainer:{
        flex:0.25,
        backgroundColor:'#F2F2F2',
        borderRadius:5,
        flexDirection:'row',
        margin:3,
        height:90,
        borderWidth:0.1,
        borderColor:'#CBCACF',
        marginBottom:10
    },
    text:{
        fontSize:15,
        color:'#88878C'
    },
   
  });