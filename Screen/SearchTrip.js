import React, { Component } from 'react'
import {View,Text,TextInput,ScrollView,StyleSheet} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import CustomContent from '../Component/CustomContent'
import CustomButton from '../Component/CustomButton'

export default class SearchTrip extends Component {
    constructor(props){
        super(props)
        this.state={
            button1:false,
            button2:false,
            button3:false
        }
        // _buttonOne=()=>{
        //     this.setState({button1:true})
        // }
        // _buttonTwo=()=>{
        //     this.setState({button2:true})
        // }
        // _buttonThree=()=>{
        //     this.setState({button3:true})
        // }

    }
    render() {
        return (
        <View style={{flex:1}}>
            <View style={{flex:0.15,justifyContent:'center',marginLeft:15}}>
                <Text style={{fontSize:30,fontWeight:'bold'}}>Trips</Text>
            </View>
            <View style={styles.searchContainer}>
                <View style={styles.s1}>
                    <View style={styles.s2}>
                        <View style={styles.s3}>
                            <Icon style={{marginLeft:10,marginRight:10}} name={'ios-search'} size={25} /> 
                        </View>
                        <View style={styles.s4}>
                            <TextInput style={styles.s5} placeholder={'Search'}/>
                        </View>
                    </View>
                </View>
                <View style={{flex:0.25,justifyContent:'center',alignItems:'center'}}>
                    <Text style={{fontSize:24,fontWeight:'bold',color:'#548CD7'}}>Cancel</Text>
                </View>
            </View>

          <View style={{flex:0.2,justifyContent:'center'}}>
                    <View style={styles.buttonContainer}>
                        <View style={{
                            flexDirection:'row',
                            justifyContent:'space-between'}}>
                            <CustomButton text={2019} color={'#0075FF'}/>
                            <CustomButton text={2020} />
                            <CustomButton text={2021}/>
                        </View>
                    </View>
            </View>

          <ScrollView style={{flex:0.55}}>
              <CustomContent date={'MAY 18-23, 2019'} />
              <CustomContent date={'MAY 25-30, 2019'}/>
              <CustomContent date={'JUN 1-6, 2019'}/>
              <CustomContent date={'JUN 8-13, 2019'}/>
          </ScrollView>
      </View>
        )
    }
}
const styles = StyleSheet.create({ 
    searchContainer:{
        flex:0.1,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        
    },
    s1:{
        flex:0.75,
        marginLeft:10,
        marginRight:5,
        
    },
    s2:{
        backgroundColor:'#DCDBE0',
        height:45,
        borderRadius:6,
        flexDirection:'row',
        
    },
    s3:{
        alignItems:'flex-start',
        justifyContent:'center',
        
    }, 
    s4:{
        justifyContent:'center',
        alignItems:'flex-end',
      
    },
    s5:{
        color:'#76757A',
        fontSize:20
    },
    buttonContainer:{
        height:45,
        backgroundColor:'#FFFEF8',
        borderWidth:3,
        borderColor:'#7CA4BF',
        justifyContent:'center',
        borderRadius:6,
        margin:10
    },
  });