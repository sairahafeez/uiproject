import React, { Component } from 'react'
import {View,Text,TouchableOpacity,StyleSheet} from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign'
import CustomContent from '../Component/CustomContent'
import CustomButton from '../Component/CustomButton'

export default class TripScreen extends Component {
    render() {
        return (
           <View style={{flex:1,backgroundColor:'#FFFFFF'}}>
               <View style={{flex:0.1,flexDirection:'row'}}>
                    <View style={{
                            flex:0.9,
                            justifyContent:'center',
                            alignItems:'center'}}>
                            <Text style={{fontSize:30}}>Trips</Text>
                    </View>
                    <View style={{
                            flex:0.1,
                            justifyContent:'center',
                            alignItems:'center'}}>
                            <Icon name={'plus'} size={30} style={{color:'#0075FF'}}/>
                    </View>
               </View>
               <View style={{flex:0.1,justifyContent:'center'}}>
                    <View style={styles.buttonContainer}>
                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                            <CustomButton text={2019} color={'#0075FF'}/>
                            <CustomButton text={2020} />
                            <CustomButton text={2021}/>
                        </View>
                    </View>
               </View>
               <View style={{flex:0.8,marginTop:15}}>
                    <CustomContent date={'MAY 18-23, 2019'}/>
                    <CustomContent date={'MAY 25-30, 2019'}/>
                    <CustomContent date={'JUN 1-6, 2019'}/>
                    <CustomContent date={'JUN 8-13, 2019'}/>
                </View>
           </View>
        )
    }
}
const styles = StyleSheet.create({ 
    buttonContainer:{
        height:45,
        backgroundColor:'#FFFEF8',
        borderWidth:3,
        borderColor:'#7CA4BF',
        justifyContent:'center',
        borderRadius:6,
        margin:10
    },
  });